#!/bin/bash

### run it with "qsub -t 1-20 batch_example2.sh" #########
# it will add in the queue 20 tasks with different PBS_ARRAYID (from 1 to 20 including)

#jobs's name
#PBS -N test

#we request 1 core

#PBS -l nodes=1:ppn=1 

#The maximum wall-clock time during which this job can run (sec) 
#PBS -l walltime=600

# output log file name
#PBS -o "run-log.txt"  

#output error file name 
#PBS -e "run-err.txt"   

# change to submission jobs directory                                                                                          
cd $PBS_O_WORKDIR

#$PBS_ARRAYID - id of this task 
python ../code.py ${PBS_ARRAYID}
