#!/bin/bash
#jobs's name
#PBS -N test

#we request 1 core
#PBS -l nodes=1:ppn=1 

#We ask for 600 sec
#PBS -l walltime=600

# output log file name
#PBS -o "run-log.txt"  

#output error file name 
#PBS -e "run-err.txt"   

# change to submission jobs directory                                                                                          
cd $PBS_O_WORKDIR
python ../code.py 1

