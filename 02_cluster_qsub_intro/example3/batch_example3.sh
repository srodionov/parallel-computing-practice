#!/bin/bash
#jobs's name
#PBS -N test

#we request 1 nodes with 8 cores per nodes
#PBS -l nodes=1:ppn=8 

#We ask 600 sec 
#PBS -l walltime=600

# output log file name
#PBS -o "run-log.txt"  

#output error file name 
#PBS -e "run-err.txt"   

# change to submission jobs directory                                                                                          
cd $PBS_O_WORKDIR

seq 1 20 | parallel python ../code.py {}
