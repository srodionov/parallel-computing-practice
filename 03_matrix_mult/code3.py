#!/usr/bin/env python

import numpy as np
import mkl
import time
import argparse

# Parse command line arguments                                                                                                
parser = argparse.ArgumentParser()
parser.add_argument("size",        help="Size of matrix", type=int);
args = parser.parse_args()

N = args.size

for i in range(5):
    A = np.random.rand (N, N)
    B = np.random.rand (N, N)
    start = time.time()
    C     = np.dot(A,B)
    end   = time.time()
    print (mkl.get_max_threads(), end - start)
