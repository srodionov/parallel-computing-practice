#!/usr/bin/env python
import numpy as np
import sys
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("seed",        help="Random seed", type=int);
parser.add_argument("-size",       help="Size of matrix", type=int, default=1000);
args = parser.parse_args()

np.random.seed(args.seed)
A = np.random.rand (args.size, args.size)
B = np.random.rand (args.size, args.size)
C = np.dot(A,B)

plt.matshow(C)

fname = "out_" + str(args.seed) + ".png"
plt.savefig(fname)
print (fname)
