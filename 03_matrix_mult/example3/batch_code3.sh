#!/bin/bash
#jobs's name
#PBS -N test

#we request 1 nodes with 1 cores per nodes = 1 x 12 = 12 cores in total
#PBS -l nodes=1:ppn=12 

# we ask for 600 sec
#PBS -l walltime=600

# output log file name
#PBS -o "run-log.txt"  

#output error file name 
#PBS -e "run-err.txt"   

# change to submission jobs directory                                                                                          
cd $PBS_O_WORKDIR

rm -f times_100.txt
for N in `seq 1 12`
do
    export OMP_NUM_THREADS=$N
    python ../code3.py 100 >> times_100.txt
done


rm -f times_2000.txt
for N in `seq 1 12`
do
    export OMP_NUM_THREADS=$N
    python ../code3.py 2000 >> times_2000.txt
done


