#!/usr/bin/env python
import numpy as np
import argparse


# Parse command line arguments                                                                                                 
parser = argparse.ArgumentParser()
parser.add_argument("input",       help="Input two column file: ncore time");
args = parser.parse_args()

tdict = {}
with open(args.input) as f:
    for line in f:
        sline = line.split()
        if (len(sline) != 2):
            raise Exception('Input is not a two column file');
        tdict[int(sline[0])] = float(sline[1])

for n in sorted(tdict):
    t = tdict[n];
    s = tdict[1]/t;
    print ("%2d %10.5g %10.3g" % (n, t, s))
