#!/bin/bash
#jobs's name
#PBS -N test

#we request 1 nodes with 12 cores per nodes = 1 x 12 = 12 cores in total
#PBS -l nodes=1:ppn=12 

# We ask for 7200 seconds
#PBS -l walltime=7200

# output log file name
#PBS -o "run-log.txt"  

#output error file name 
#PBS -e "run-err.txt"   

# change to submission jobs directory                                                                                          
cd $PBS_O_WORKDIR

rm -f times.txt
for N in `seq 1 12`
do
    export OMP_NUM_THREADS=$N
    echo -n "$N " >> times.txt
    /usr/bin/time -f %e python ../code2.py -size=5000 ${N} 2>> times.txt
done

