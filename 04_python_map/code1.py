#!/usr/bin/env python
import numpy as np
import argparse

# Parse command line arguments                                                                                                
parser = argparse.ArgumentParser()
parser.add_argument("Nm", help="Number of matrices", type=int);
args = parser.parse_args()

Nm = args.Nm

def fun(i):
    np.random.seed(i)
    A = np.random.rand (100, 100)
    B = np.random.rand (100, 100)
    return np.sum(np.dot(A, B))

rez = 0
for i in range(Nm):
    rez += fun(i)

# it is equivalent to rez = np.sum( map(fun, range(Nm)) )

print (rez)
