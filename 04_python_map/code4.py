#!/usr/bin/env python
import numpy as np
import argparse
from multiprocessing import Process, Queue, cpu_count
import mkl

# To be sure that we use 1 thread in MKL
mkl.set_num_threads(1)

# function wait order from orderq
# stop if order < 0, otherwise make computation and put result in the rezq
def fun(orderq, rezq):
    while True:
        order = orderq.get();
        if (order < 0):
            return
        np.random.seed(order)
        A = np.random.rand (100, 100)
        B = np.random.rand (100, 100)
        rezq.put(np.sum(np.dot(A, B)))


# Parse command line arguments                                                                                                
parser = argparse.ArgumentParser()
parser.add_argument("Nm", help="Number of matrices", type=int);
parser.add_argument("-ncores", help="Number of cores to use", type=int);
args = parser.parse_args()

Nm      = args.Nm
ncores  = args.ncores

if (ncores == None):
    ncores = cpu_count()
        
#Adjust number of cores if necessary
if (ncores > Nm):
    ncores = Nm

# Make order and result queues
orderq = Queue()
rezq   = Queue()    

# List of all workers
procs  = []


#Initialize all workers
for i in range(ncores):
    p      = Process(target=fun, args=(orderq, rezq))    
    p.start()
    procs.append(p)

# Put all jobs in orderq
for i in range(Nm):
    orderq.put(i);

rez = 0
# Recive all results
for i in range(Nm):
    rez += rezq.get()

#stop all workers
for i in range(ncores):
    orderq.put(-1)

#Join all workers
for p in procs:
    p.join()

print (rez)
