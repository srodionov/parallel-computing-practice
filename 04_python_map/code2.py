#!/usr/bin/env python
import numpy as np
import argparse
from multiprocessing import Pool
import mkl

# To be sure that we use 1 thread in MKL
mkl.set_num_threads(1)

# Parse command line arguments                                                                                                
parser = argparse.ArgumentParser()
parser.add_argument("Nm", help="Number of matrices", type=int);
parser.add_argument("-ncores", help="Number of cores to use", type=int);
args = parser.parse_args()

Nm = args.Nm

def fun(i):
    np.random.seed(i)
    A = np.random.rand (100, 100)
    B = np.random.rand (100, 100)
    return np.sum(np.dot(A, B))

p   = Pool(args.ncores);
rez = np.sum( p.map(fun, range(Nm)) )

print (rez)

p.close()
p.join()
