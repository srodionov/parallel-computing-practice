#!/bin/bash

### run it with "qsub -q mpi batch_mpi.sh" #########

#jobs's name
#PBS -N test_MPI

#we request 2 nodes and 6 cores per nodes for a total of 2 x 6 = 12 cores

#PBS -l nodes=2:ppn=6 

#The maximum wall-clock time during which this job can run (sec) 
#PBS -l walltime=60

# output log file name
#PBS -o "run-log.txt"  

#output error file name 
#PBS -e "run-err.txt"   

# change to submission jobs directory                                                                                          
cd $PBS_O_WORKDIR

# save list of nodes used during the run
cat ${PBS_NODEFILE} > nodes_list.txt

# command to launch the run
mpiexec -f ${PBS_NODEFILE} -np ${PBS_NP} ./mpi_array
