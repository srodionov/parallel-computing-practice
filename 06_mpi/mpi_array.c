#include <mpi.h>   // PROVIDES THE BASIC MPI DEFINITION AND TYPES
#include <stdio.h>
#include <string.h>
#define NMAX 24
#define MIN(a,b) ((a) < (b) ? (a) : (b))

int main(int argc, char **argv) {
  int i, my_rank, partner, size,a[NMAX],chunk,istart,istop;
  MPI_Status stat;
  
  MPI_Init(&argc, &argv);                  // START MPI                           
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); // DETERMINE RANK OF THIS PROCESSOR   
  MPI_Comm_size(MPI_COMM_WORLD, &size);    // DETERMINE TOTAL NUMBER OF PROCESSORS
  
  chunk=NMAX/size;              // #chunks per processors
  istart=(chunk*my_rank);       // first index of my rank
  istop=MIN(istart+chunk,NMAX); // last index of my rank

  for (i=istart; i<istop; i++)  // EVERY PROCESSOR COMPUTE ONE CHUNK  OF THE ARRAY
    a[i] = 2 * i;

  if (my_rank == 0) { // master GATHER ALL RESULTS
    for (partner = 1; partner < size; partner++){
      istart=(chunk*partner); 
      istop=MIN(istart+chunk,NMAX);

      MPI_Recv(a + istart ,istop-istart, MPI_INT, partner, 1, MPI_COMM_WORLD, &stat);
    }
    for (i=0; i<NMAX; i++)
      fprintf(stderr,"a[%5d] = %8d\n",i,a[i]);
  }
  else {             // ALL processors except the master
    MPI_Send(a+istart,istop-istart , MPI_INT, 0,1,MPI_COMM_WORLD);
  }
  MPI_Finalize();  // EXIT MPI
}
