#!/usr/bin/env python
import numpy as np
import sys
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# Slow matrix multiplication
def mult(A,B):
    n = len(A);
    C = np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            for k in range(n):
                C[i,j] += A[i,k] * B[k,j]                 
    return C

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("seed",        help="Random seed", type=int);
parser.add_argument("-size",       help="Size of matrix", type=int, default=100);
args = parser.parse_args()

np.random.seed(args.seed)
A = np.random.rand (args.size, args.size)
B = np.random.rand (args.size, args.size)
C = mult(A,B)

plt.matshow(C)

fname = "out_" + str(args.seed) + ".png"
plt.savefig(fname)
print (fname)
