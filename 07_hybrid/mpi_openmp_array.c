#include <mpi.h>   // PROVIDES THE BASIC MPI DEFINITION AND TYPES
#include <stdio.h>
#include <string.h>
#define NMAX 24
#define MIN(a,b) ((a) < (b) ? (a) : (b))

int main(int argc, char **argv) {
  int i, my_rank, partner, size,a[NMAX],chunk,istart,istop;
  int this_thread,     // my thread index
      num_threads;     // #threads used during the run

  MPI_Status stat;

  MPI_Init(&argc, &argv);                  // START MPI                           
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); // DETERMINE RANK OF THIS PROCESSOR   
  MPI_Comm_size(MPI_COMM_WORLD, &size);    // DETERMINE TOTAL NUMBER OF PROCESSORS

#pragma omp parallel 
  {
    this_thread = omp_get_thread_num();
    num_threads = omp_get_num_threads();

    if (this_thread == 0) { // Only thread #0 print this
      printf("Proc [%d] => #Threads used = %d\n",my_rank, num_threads);
    }
  }  


  chunk=NMAX/size;              // #chunks per processors
  istart=(chunk*my_rank);       // first index of my rank
  if (my_rank<size-1) {
    istop=MIN(istart+chunk,NMAX); // last index of my rank
  } else {
    istop=NMAX;
  }

  #pragma omp parallel for
  for (i=istart; i<istop; i++)  // EVERY PROCESSOR COMPUTE ONE CHUNK  OF THE ARRAY
    a[i] = 2 * i;

  if (my_rank == 0) { // master GATHER ALL RESULTS
    printf("Total numbers of Nodes : [%d]\n",size);

    for (partner = 1; partner < size; partner++){
      istart=(chunk*partner); 
      if (partner<size-1) {
        istop=MIN(istart+chunk,NMAX);
      } else {
        istop=NMAX;
      }
      MPI_Recv(a + istart ,istop-istart, MPI_INT, partner, 1, MPI_COMM_WORLD, &stat);
    }
    for (i=0; i<NMAX; i++)
      fprintf(stderr,"a[%5d] = %8d\n",i,a[i]);
  }
  else {             // ALL processors except the master
    MPI_Send(a+istart,istop-istart , MPI_INT, 0,1,MPI_COMM_WORLD);
  }
  MPI_Finalize();  // EXIT MPI
}
