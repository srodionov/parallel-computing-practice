#!/bin/bash

### run it with "qsub -q mpi batch_mpi.sh" #########

#jobs's name
#PBS -N test_Hybrid_OpenMP_MPI

#we request 2 nodes and 5 cores per nodes for a total of 2 x 5 = 10 cores
#there will be 2 MPI process with 5 OpenMP tasks

#PBS -l nodes=3:ppn=5 

#The maximum wall-clock time during which this job can run (sec) 
#PBS -l walltime=60

# output log file name
#PBS -o "run-log.txt"  

#output error file name 
#PBS -e "run-err.txt"   

# change to submission jobs directory                                                                                          
cd $PBS_O_WORKDIR

# save list of nodes used during the run
cat ${PBS_NODEFILE} > nodes_list.txt

# --> create list of nodes (MANDATORY)
uniq ${PBS_NODEFILE} > nodes.list

# MANDATORY
export OMP_NUM_THREADS=${PBS_NUM_PPN}
export IPATH_NO_CPUAFFINITY=1

# command to launch the run
mpiexec -f nodes.list  -np ${PBS_NUM_NODES} ./mpi_openmp_array
