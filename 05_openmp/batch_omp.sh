#!/bin/bash

### run it with "qsub -q batch batch_omp.sh" #########

#jobs's name
#PBS -N test_OpenMP

#we request 1 node and 4 threads

#PBS -l nodes=1:ppn=4 

#The maximum wall-clock time during which this job can run (sec) 
#PBS -l walltime=60

# output log file name
#PBS -o "run-log.txt"  

#output error file name 
#PBS -e "run-err.txt"   

# change to submission jobs directory                                                                                          
cd $PBS_O_WORKDIR

./omp_array
