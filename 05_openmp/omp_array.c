#include <omp.h>
#include <stdio.h>

#define NMAX 1000000

int main(int argc, char **argv)
{
 int this_thread,     // my thread index
     num_threads;     // #threads used during the run
 int a[NMAX],i;
 
 #pragma omp parallel 
 {
  this_thread = omp_get_thread_num();
  num_threads = omp_get_num_threads();
  
  if (this_thread == 0) { // Only thread #0 print this
    printf("#Threads used = %d\n", num_threads);
  }
 }  

 #pragma omp parallel for
 for (i = 0; i < NMAX; i++) {
  a[i] = 2 * i ;
 }

 return 0;
}
